let dockerSecrets = require('docker-swarm-secrets');

let secretReadOptions = {
    secretsDir: '/' // allow secrets to be passed in as fully qualitifed paths
};

export default {
    controllerUrl: 'http://10.1.1.8/',
    mqtt: {
        host: (process.env.mqtt_protocol || 'mqtt://') + (process.env.mqtt_host || '10.0.0.4'),
        port: process.env.mqtt_port || 1883,
        username: process.env.mqtt_username || 'lighting',
        password: dockerSecrets.readSecretSync(process.env.mqtt_password_file, secretReadOptions) || 'lighting',
        topicRoot: '/home/audio/axium-controller/zone/'
    }
};