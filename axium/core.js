import utils from './utils';
import state from './state';
import axios from 'axios';

let axiosInstance;
let axiosLongPollInstance;

async function connectToAmp(controllerUrl) {

	axiosInstance = axios.create({
		baseURL: controllerUrl,
		timeout: 2000,
		headers: {'Content-Type': 'application/x-axium'}
	});
	
	axiosLongPollInstance = axios.create({
		baseURL: controllerUrl,
		timeout: 30000,
		headers: {'Content-Type': 'application/x-axium'}
	});

	let requestAmpDetails = "14FF02\r\n";
	for (var preset = 1; preset <= 14; preset++)
		requestAmpDetails += "2BFF" + utils.to_hex(preset+1) + "\r\n";

	await makeRequest(
		requestAmpDetails,
		processCommandResponse
	);

	await refreshZoneDetails();
	state.printState();

	startLongPollProcessor();
}

async function setZoneOnState(zoneId, isOn) {
	sendZoneCommand(0x01, zoneId, [isOn ? 1 : 0]);
}

async function setZoneMuteState(zoneId, isMuted) {
	sendZoneCommand(0x02, zoneId, [isMuted ? 0 : 1]);
}

async function setZoneVolume(zoneId, volume) {
	volume = Math.max(0, Math.min(100, volume));
	let volumeValue = Math.ceil(0xA0 * volume / 100);
	sendZoneCommand(0x04, zoneId, [volumeValue]);
}

async function startLongPollProcessor() {

	try {
		let response = await axiosLongPollInstance.get('axiumlong.cgi'); // TODO - pass timeout value
		if (response.status == 200 && response.data) {
			console.log('received data from long poll: ' + response.data);
			await processAxiumResponse(response.data, processCommandResponse);
		}
		else {
			console.log('long poll returned with status code:' + response.status + ' and data ' + response.data);
		}
			
	} catch (error) {
		console.error('long poll error: ' + error);
	}
	
	setTimeout(startLongPollProcessor, 1);

}

// Sends a command, responseHandler is optional
async function makeRequest (command, responseHandler)
{

	try {
		let response = await axiosInstance.post('axium.cgi', command);
		if (response.headers['set-cookie']) {
			const cookie = response.headers['set-cookie'];
			axiosInstance.defaults.headers.common.Cookie = cookie;
			axiosLongPollInstance.defaults.headers.common.Cookie = cookie;			
		}

		if (responseHandler) {
			await processAxiumResponse(response.data, responseHandler);
		}
			
	} catch (error) {
		console.error(error);
		throw error;
	}
}

async function refreshZoneDetails() {

	for (const zone of state.getZones()) {
		// Request zone name, status, muted status, and volume:
		console.log('Getting details for zone ' + zone.id);

		const encodedZone = utils.encodeZone(zone.id);

		// get zone name:
		await makeRequest(
			"38" + encodedZone + "\r\n",
			processCommandResponse);

		// get zone status:
		await makeRequest(
			"01" + encodedZone + "\r\n",
			processCommandResponse);

		// Get is muted	status:
		await makeRequest(
			"02" + encodedZone + "\r\n",
			processCommandResponse);

		// Get volume:
		await makeRequest(
			"04" + encodedZone + "\r\n",
			processCommandResponse);
							
	}
}

// parameters is an array of values 0..255
async function sendZoneCommand (command, zone, parameters)
{
	var requestString = utils.to_hex (command) + utils.encodeZone(zone);
	for (var n = 0; n < parameters.length; n++)
		requestString += utils.to_hex (parameters[n]);
	await makeRequest (requestString + "\r\n", processCommandResponse);
}

async function processCommandResponse(command, zone, data)
{
	try {
		switch (command) {
			case 0x94 :	// device information response		
				if (data.length >= 5) {
					if (data[0] == 0) {	// amplifier
						let amp_type = data[2];
						let unit_id = (data[3] << 8) | data[4];
						let firmware = data[1];

						state.setZoneAmpDetails(zone, amp_type, unit_id, firmware);					
					}
				}
				break;
			case 0x01 :	// standby
				state.setZoneOnState(zone, data[0] == 1);
				break;
			case 0x30 :	// zone linking
				console.log('zone linking recieved for ' + zone);
				break;
			case 0x1C :	// zone name
				let zoneName = utils.decode_utf8_array (data);
				state.setZoneName(zone, zoneName);
				break;
			case 0x2A :	// preset name
				// var preset = data.shift() - 1;
				// if (data.length > 0) {
				// 	let presetName = utils.decode_utf8_array (data);
				// 	console.log('preset ' + preset + ' is named ' + presetName);
				// }
				break;
			case 0x02 :		// mute
				let isMuted = data[0] == 0;
				state.setZoneIsMuted(zone, isMuted);
				break;
			case 0x04 :		// volume
				let volume = Math.floor (data[0] * 100 / 0xA0);
				state.setZoneVolume(zone, volume);
				break;
			case 0x29 :		// source name
				break;
			case 0x30 :		// link zones
				break;
			case 0x3C :		// available sources: check visibility of extended sources
				break;
			case 0x3E :		// play status notification
				break;				
		}
	}
	catch (err) {
		console.log("processCommandResponse error: " + err);
	}
}	// processCommandResponse

async function processAxiumResponse (command, handlerCallback)
{
	//console.log(command.replace(/\r\n/g, " "));

	// Process the command in all objects that may be interested in it.
	while (command != "") {
		var cmd_len = command.indexOf ("\n");
		if (cmd_len == -1) {
			// Shouldn't happen otherwise data are lost.
			console.error("Dropping command fragment: " + command);
			break;
		}
		if (cmd_len > 4) {
			var num_data_bytes = ((command.charAt(cmd_len - 1) == '\r' ? cmd_len - 1 : cmd_len) >> 1) - 2;
			var cmd_number = utils.get_hex_byte (command, 0);
			var zone = utils.decode_zone (utils.get_hex_byte (command, 2));
			if (zone == null) {
				continue;
			}

			var data_bytes = new Array(num_data_bytes);
			for (let n = 0; n < num_data_bytes; n++) {
				data_bytes[n] = utils.get_hex_byte (command, 4 + n * 2);
			}

			//console.log('processAxiumResponse for zone ' + zone + ' command: ' + cmd_number + ' data: ' + data_bytes);
			
			await handlerCallback (cmd_number, zone, data_bytes);
		}
		command = command.substr(cmd_len + 1);		// get next command
	}
}

export default {
	connectToAmp,
	setZoneOnState,
	setZoneMuteState,
	setZoneVolume
};
