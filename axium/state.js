const events = require('events');

const eventEmitter = new events.EventEmitter();

let zones = [];

function getZone(zoneIndex) {
    if (!zones[zoneIndex]) {
        zones[zoneIndex] = {
            id: zoneIndex,
            name: zoneIndex + '',
            ampDetails: {}
        };
    }    
    return zones[zoneIndex];
}

function getZones() {
    return zones.filter(z => !!z);
}

export default {
    setZoneAmpDetails: (zoneId, ampType, unitId, firmware) => {
        const zone = getZone(zoneId);
        zone.ampDetails = {
            ampType,
            unitId,
            firmware
        };

        eventEmitter.emit('changed', 'ampDetails', zone);
    },

    setZoneOnState: (zoneId, isOn) => {
        console.log('setting zone ' + zoneId + ' on status to ' + isOn);
        const zone = getZone(zoneId);
        zone.isOn = isOn;

        eventEmitter.emit('changed', 'isOn', zone);
    },

    setZoneName: (zoneId, name) => {
        console.log('setting zone ' + zoneId + ' name to ' + name);
        const zone = getZone(zoneId);
        zone.name = name;

        eventEmitter.emit('changed', 'name', zone);
    },

    setZoneIsMuted: (zoneId, isMuted) => {
        console.log('setting zone ' + zoneId + ' isMuted to ' + isMuted);
        const zone = getZone(zoneId);
        zone.isMuted = isMuted;

        eventEmitter.emit('changed', 'isMuted', zone);
    },

    setZoneVolume: (zoneId, volume) => {
        console.log('setting zone ' + zoneId + ' volume to ' + volume);
        const zone = getZone(zoneId);
        zone.volume = volume;

        eventEmitter.emit('changed', 'volume', zone);
    },

    getZone,

    getZones,

    printState: () => {
        for (const z of getZones()) {
            console.log(z);
        }
    },

    getEventEmitter: () => {
        return eventEmitter;
    }

};

