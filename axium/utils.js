
function to_hex(val)
{
    val = Number(val);
    if (val < 0)
        val += 256;		// negatives are 0x80..0xFF
    val = val.toString(16).toUpperCase();
    if (val.length == 1)
        return "0" + val;
    return val;
}

function decode_zone (zone)
{
    if (zone != 0xFF) {
        zone &= ~0x20;		// mask out preset bit
        if ((zone & 0xC0) == 0x80)
            zone = 32 + (zone & 0x1F);		// Extended zones 32..63
        else if ((zone & 0xC0) == 0xC0)
            zone = 64 + (zone & 0x1F);		// Extended zones 64..95
        else if ((zone & 0xC0) == 0x40)
            return null;		// preamp zone: not supported
    }
    return zone;
}

function encode_zone (zone)
{
    if (zone != 0xFF) {
        if (zone >= 64)
            zone = 0xC0 + (zone - 64);
        else if (zone >= 32)
            zone = 0x80 + (zone - 32);
    }
    return zone;
}

function decode_source (source)
{
    switch (source) {
        case 0 : return 4;
        case 1 : return 5;
        case 2 : return 6;
        case 3 : return 3;
        case 4 : return 7;
        case 5 : return 0;
        case 6 : return 1;
        case 7 : return 2;
        default : return source;
    }
    return null;
}

function encode_source (source)
{
    //!! support extended sources
    switch (source) {
        case 4 : return 0;
        case 5 : return 1;
        case 6 : return 2;
        case 3 : return 3;
        case 7 : return 4;
        case 0 : return 5;
        case 1 : return 6;
        case 2 : return 7;
        default : return source;
    }
    return null;
}

function get_hex_byte(s_hex, start)
{
    return parseInt (s_hex.substr (start, 2), 16);
}

// Returns a string from a byte array of UTF-8 codes
// Note: only handles a subset of the full range of utf-8 codes
function decode_utf8_array (codes)
{
    var result = "";
    var i = 0;
    var c, c2, c3;

    while (i < codes.length) {
        c = codes[i++];
        if (c == 0)
            break;
        if (c < 128) {
            // U-00000000 � U-0000007F: 0ccccccc (standard ASCII)
            result += String.fromCharCode (c);
        }
        else {
            c2 = codes[i++];
            if ((c > 191) && (c < 224)) {
                // U-00000080 � U-000007FF: 110ccccc 10cccccc
                result += String.fromCharCode (((c & 31) << 6) | (c2 & 63));
            }
            else {
                c3 = codes[i++];
                // U-00000800 � U-0000FFFF: 1110cccc 10cccccc 10cccccc
                result += String.fromCharCode (((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            }
        }
    }
    return result;
}	// decode_utf8_array

function encodeZone(zone)
{
	return to_hex(encode_zone(zone));
}

export default {
    to_hex,
    decode_zone,
    encode_zone,
    decode_source,
    encode_source,
    get_hex_byte,
    decode_utf8_array,
    encodeZone
};