import MQTT from 'async-mqtt';
import nodeCleanup from 'node-cleanup';

import config from './config/local';
import core from './axium/core';
import state from './axium/state';

const topicRoot = config.mqtt.topicRoot;

// Connect to MQTT:
const client = MQTT.connect(
    config.mqtt.host, 
    {
				port: config.mqtt.port,
        username: config.mqtt.username, 
        password: config.mqtt.password
    }
);
client.on('connect', setUpSubscriptions);
client.on('message', onMessage);

nodeCleanup(function () {
	client.end();
});

// Subscribe to changes to the internal state model of the amplifier zones, so we can update the MQTT topics
var stateEvents = state.getEventEmitter();
stateEvents.on('changed', async function (property, zone) {
	console.log('received event - zone ' + zone.id + ' changed state of ' + property + ' to ' + JSON.stringify(zone[property]));

	let payload = zone[property] + '';
	if (payload == {}.toString()) {
		payload = JSON.stringify(zone[property]);
	} 
	
	await client.publish(
		topicRoot + zone.id + '/' + property, 
		payload,
		{ retain: true}
	);
});


core.connectToAmp(config.controllerUrl);

async function setUpSubscriptions() {

	console.log('Starting');
	try {
		await client.subscribe(topicRoot + '+/+/set');
	} catch (e) {
		console.error(e);
		console.error(e.stack);
		process.exit();
	}
}

async function onMessage(topic, message) {
	console.log('received message on topic ' + topic + ' of value: ' + message.toString());

	const topicComponents = topic.substring(topicRoot.length).split('/');
	if (topicComponents.length != 3 || topicComponents[2] != 'set') {
		console.error('Not able to handle topic: ' + topic);
		return;
	}

	const zoneId = topicComponents[0];
	const property =  topicComponents[1];
	const value = message.toString();
	console.log('Received set command for zone ' + zoneId + ' and property ' + property + ' with value ' + value);

	switch (property) {
		case 'isOn':
			core.setZoneOnState(zoneId, value.toLowerCase() == 'true');
			break;
		case 'isMuted':
			core.setZoneMuteState(zoneId, value.toLowerCase() == 'true');
			break;
		case 'volume':
			const volume = parseInt(value, 10);
			if (!isNaN(volume)) {
				core.setZoneVolume(zoneId, volume);
			}
			break;
		default:
			console.warn('modification of property' + property + ' is not yet supported');
			break;
	}
	
}