FROM node:8-alpine as nodejs

#
# STAGE: INSTALL PACKAGES
#
FROM nodejs as installer
ENV NODE_ENV production
WORKDIR /axium
# copy required dependency manifest files:
COPY package*.json* ./
# install packages:
RUN yarn

#
# STAGE: Copy source
#
FROM installer as copy
ENV NODE_ENV production
WORKDIR /axium
COPY . .

#
# STAGE: Run
#
FROM copy as run
ENV NODE_ENV production
WORKDIR /axium

# Run the app
ENTRYPOINT ["yarn", "start"]
