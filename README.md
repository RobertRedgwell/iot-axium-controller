# NodeJS Axium Amplifier command interface
This is an application that interfaces with the CGI web application hosted on Axium amplifiers (http://www.axiumcontrol.com/amplifiers.html).

This enables smart home devices to publish and subscribe to MQTT topics to get and set zone standby state, mute state, and volume.
Other information is also published to MQTT topics such as zone name, ampType, unitId, and firmware.

The code is based heavily on the browser implementation found in the web application served by the Axium amplifier.

## Set up
To set up the application, you'll need node 8 (or later) and yarn.
Once you have those installed:

1. Run `yarn install`
1. Modify the configuration in the config/local.js file to suit your environment

## Running
To run the application, just run `yarn start`.

## Building and running the docker container
The application can also be run from a docker container, if you don't want to install node or yarn locally:

1. `docker build --rm -f "Dockerfile" -t axium-amp-interface:latest ./`
1. `docker run --rm -it axium-amp-interface:latest`